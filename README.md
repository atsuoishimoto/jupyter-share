# Jupyter viewer

Gitlab pageを使った Jupyter Notebook共用

サンプルサイト -> https://atsuoishimoto.gitlab.io/jupyter-share/

1. ssh-keygenでキーを生成する
2. Settings -> Pipeliness -> Secret variables で、`SSH_PRIVATE_KEY` という名前の変数を作成して、SSH秘密鍵をVALUEに書き込む
3. Settings -> Repository -> Deploy Keys で、公開鍵を追加する。このとき、`Write access allowed` をチェックして書き込み可能にする。
4. contentsディレクトリに、Jupyter notebook(.ipynb)ファイルを追加する
5. しばらく待つ
6. Gitlab pages を見に行く (URLは `Settings` の `Pages` を参照)
7. たまに Github pages の更新が失敗するので、`pipelines` ページで `Run Pipeline` をクリックする。

